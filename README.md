# Talks

The repository with the slides and other materials to my talks.

|     |     |     |
| --- | --- | --- |
| 27. 2. 2019 | [Pyvo Olomouc](https://pyvo.cz/olomouc-pyvo/2019-02/) | [slides](./pyvo-2019-02-scribus/scribus.pdf) |
| 27. 2. 2020 | [Pyvo Olomouc](https://pyvo.cz/olomouc-pyvo/2020-02/) | [slides](./pyvo-2020-02-ogr/pyvo-2020-02-ogr.ipynb) |

