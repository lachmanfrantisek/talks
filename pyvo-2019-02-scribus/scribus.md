<!--
$theme: gaia
template: invert
-->

# Python
# in

# ![150%](https://test.scribus.net/wp-content/uploads/2015/05/scribus_header-91.png)

#### František Lachman
###### flachman@redhat.com

###### PYVO, Olomouc 27. 2. 2019


---
# Overview
- Scripter
- Scribus Skripter
- Scripter API
- Examples
- Sources

---
# ![150%](https://test.scribus.net/wp-content/uploads/2015/05/scribus_header-91.png)

![70%](./img/releases.png) ![65%](./img/commits.png)

---
# ![150%](https://test.scribus.net/wp-content/uploads/2015/05/scribus_header-91.png)

- [GitHub mirror repo](https://github.com/scribusproject/scribus)
- [Issue tracker](https://bugs.scribus.net/my_view_page.php)
- multiplatform
- stable `1.4` x unstable `1.5`
- flatpak, appimage

---

# Scripter

- `Help` (`F1`) --> `For developers` --> `Scripter`

```
import sys
print(sys.version)
```
```
2.7.15rc1 (default, Nov 12 2018, 14:31:15) 
[GCC 7.3.0]
```

---

```
newDocDialog()
```

```
my_text = createText(2, 3, 200, 10)
print(my_text)
setText("Hello world", my_text)
```

```
rect_name = createRect(20, 20, 20, 20)
setFillColor("Black", rect_name)
```

... more in docs

---


---
# Examples

- [scribus wiki - Full functional scripts](https://wiki.scribus.net/canvas/Category:Scripts#Full_functional_scripts)
- [Calendar wizard (included in scribus)](https://wiki.scribus.net/canvas/CalendarWizard)
- https://gitlab.com/lachmanfrantisek/scribus-family-calendar/

---
# Overview
- Scribus
- Scribus Scripter
- Scripter API
- Examples
- Sources

---

# ?!?