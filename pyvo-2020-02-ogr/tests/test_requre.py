import os
from requre import RequreTestCase
from ogr import GithubService


class FactoryTests(RequreTestCase):
    def setUp(self):
        super().setUp()
        github_token = os.environ.get("GITHUB_TOKEN")
        self.github_service = GithubService(token=github_token)

    def test_get_project_github(self):
        project = self.github_service.get_project(
            namespace="packit-service",
            repo="ogr",
        )
        assert project.github_repo
        assert project.get_issue_list()
        assert project.get_description() == "One Git library to Rule -- one API for many git forges"
